const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  const database = mongoClient.db('ep1-db');
  const users = database.collection('users');

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  api.post('/users', validatePostRequest, (req, res) => {
    const user = { id: uuid(), ...req.body };
    delete user.passwordConfirmation;

    dispatchCreationEvent(user);
    delete user.password;
    return res.status(201).json({user: user});
  });

  api.delete('/users/:uuid', validateDeleteRequest,(req, res) => {
    dispatchDeletionEvent(req.params.uuid);
    return res.status(200).json({});
  });

  async function validatePostRequest(req, res, next) {
    const errorResponse = {error: ''};
    const requiredFields = ['name', 'email', 'password', 'passwordConfirmation'];
    const fieldExpectedFormat = {
      'name': /[a-zA-z]+/,
      'email': /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i,
      'password': /\b[a-zA-z0-9]{8,32}\b/,
      'passwordConfirmation': /\b[a-zA-z0-9]{8,32}\b/,
    };

    for (let requiredField of requiredFields) {
      if (!req.body.hasOwnProperty(requiredField)) {
        errorResponse.error = `Request body had missing field ${requiredField}`;
        return res.status(400).json(errorResponse);
      }
    }

    for (let field of requiredFields) {
      if (!req.body[field].match(fieldExpectedFormat[field])) {
        errorResponse.error = `Request body had malformed field ${field}`;
        return res.status(400).json(errorResponse);
      }
    }

    if (req.body.password !== req.body.passwordConfirmation) {
      errorResponse.error = 'Password confirmation did not match';
      return res.status(422).json(errorResponse);
    }

    const userOnDatabase = await users.findOne({email: req.body.email});
    if (userOnDatabase) {
      errorResponse.error = 'There is already a user with this email in the system';
      return res.status(400).json(errorResponse);
    }

    next();
  }

  async function validateDeleteRequest(req, res, next) {
    const errorResponse = {error: ''};
    let authToken = req.header('Authentication');

    if (!authToken) {
      errorResponse.error = 'Access Token not found';
      return res.status(401).json(errorResponse);
    }

    authToken = authToken.replace('Bearer ', '');
    const tokenId = jwt.decode(authToken).id;
    const userId = req.params.uuid;
    if (userId !== tokenId) {
      errorResponse.error = 'Access Token did not match User ID';
      return res.status(403).json(errorResponse);
    }

    const userOnDatabase = await users.findOne({_id: userId});
    if (userOnDatabase === null) {
      errorResponse.error = 'There is no specified user registered in the system';
      return res.status(400).json(errorResponse);
    }

    next();
  }

  function dispatchCreationEvent(user) {
    const { id, ...userAggregate } = user;
    const eventPayload = {
      eventType: 'UserCreated',
      entityId: id,
      entityAggregate: userAggregate
    };

    stanConn.publish('users', JSON.stringify(eventPayload), (err, guid) => {
      if (err) {
        console.log('publish failed: ' + err);
      }
    });
  }

  function dispatchDeletionEvent(uuid) {
    const eventPayload = {
      eventType: 'UserDeleted',
      entityId: uuid,
      entityAggregate: {}
    };

    stanConn.publish('users', JSON.stringify(eventPayload), (err) => {
      if (err) {
        console.log('publish failed: ' + err);
      }
    });
  }
  /* ******************* */

  return api;
};
