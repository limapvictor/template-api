const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */
    /* YOUR CODE GOES HERE */
    /* ******************* */

    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    let isLastReceivedMessage = true;
    const subscription = conn.subscribe('users', opts);

    const database = mongoClient.db('ep1-db');
    const users = database.collection('users');

    subscription.on('message', (msg) => {
      if (isLastReceivedMessage) {
        isLastReceivedMessage = false;
        return;
      }

      const eventInfo = JSON.parse(msg.getData());
      if (eventInfo.eventType === 'UserCreated') handleCreationEvent(eventInfo.entityId, eventInfo.entityAggregate);
      if (eventInfo.eventType === 'UserDeleted') handleDeletionEvent(eventInfo.entityId);

      function handleCreationEvent(userId, userAggregate) {
        users.insertOne({
          _id: userId,
          ...userAggregate
        })
            .then((_) => console.log(`User ${userId} stored!`))
            .catch((error) => console.error(error));
      }

      function handleDeletionEvent(userId) {
        users.deleteOne({_id: userId})
            .then((deleteResult) => console.log(`${deleteResult.deletedCount} user deleted!`))
            .catch((error) => console.error(error));
      }
    });
  });

  return conn;
};
